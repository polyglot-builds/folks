require 'test_helper'

class FolksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @folk = folks(:one)
  end

  test "should get index" do
    get folks_url
    assert_response :success
  end

  test "should get new" do
    get new_folk_url
    assert_response :success
  end

  test "should create folk" do
    assert_difference('Folk.count') do
      post folks_url, params: { folk: { date_of_joining: @folk.date_of_joining, email: @folk.email, first_name: @folk.first_name, last_name: @folk.last_name, mobile_number: @folk.mobile_number, nick_name: @folk.nick_name, twitter: @folk.twitter } }
    end

    assert_redirected_to folk_url(Folk.last)
  end

  test "should show folk" do
    get folk_url(@folk)
    assert_response :success
  end

  test "should get edit" do
    get edit_folk_url(@folk)
    assert_response :success
  end

  test "should update folk" do
    patch folk_url(@folk), params: { folk: { date_of_joining: @folk.date_of_joining, email: @folk.email, first_name: @folk.first_name, last_name: @folk.last_name, mobile_number: @folk.mobile_number, nick_name: @folk.nick_name, twitter: @folk.twitter } }
    assert_redirected_to folk_url(@folk)
  end

  test "should destroy folk" do
    assert_difference('Folk.count', -1) do
      delete folk_url(@folk)
    end

    assert_redirected_to folks_url
  end
end
