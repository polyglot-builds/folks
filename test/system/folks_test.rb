require "application_system_test_case"

class FolksTest < ApplicationSystemTestCase
  setup do
    @folk = folks(:one)
  end

  test "visiting the index" do
    visit folks_url
    assert_selector "h1", text: "Folks"
  end

  test "creating a Folk" do
    visit folks_url
    click_on "New Folk"

    fill_in "Date of joining", with: @folk.date_of_joining
    fill_in "Email", with: @folk.email
    fill_in "First name", with: @folk.first_name
    fill_in "Last name", with: @folk.last_name
    fill_in "Mobile number", with: @folk.mobile_number
    fill_in "Nick name", with: @folk.nick_name
    fill_in "Twitter", with: @folk.twitter
    click_on "Create Folk"

    assert_text "Folk was successfully created"
    click_on "Back"
  end

  test "updating a Folk" do
    visit folks_url
    click_on "Edit", match: :first

    fill_in "Date of joining", with: @folk.date_of_joining
    fill_in "Email", with: @folk.email
    fill_in "First name", with: @folk.first_name
    fill_in "Last name", with: @folk.last_name
    fill_in "Mobile number", with: @folk.mobile_number
    fill_in "Nick name", with: @folk.nick_name
    fill_in "Twitter", with: @folk.twitter
    click_on "Update Folk"

    assert_text "Folk was successfully updated"
    click_on "Back"
  end

  test "destroying a Folk" do
    visit folks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Folk was successfully destroyed"
  end
end
