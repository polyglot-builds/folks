json.extract! folk, :id, :first_name, :last_name, :email, :mobile_number, :twitter, :date_of_joining, :nick_name, :created_at, :updated_at
json.url folk_url(folk, format: :json)
