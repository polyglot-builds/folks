class FolksController < ApplicationController
  before_action :set_folk, only: %i[ show edit update destroy ]

  # GET /folks or /folks.json
  def index
    @folks = Folk.all
  end

  # GET /folks/1 or /folks/1.json
  def show
  end

  # GET /folks/new
  def new
    @folk = Folk.new
  end

  # GET /folks/1/edit
  def edit
  end

  # POST /folks or /folks.json
  def create
    @folk = Folk.new(folk_params)

    respond_to do |format|
      if @folk.save
        format.html { redirect_to @folk, notice: "Folk was successfully created." }
        format.json { render :show, status: :created, location: @folk }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @folk.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /folks/1 or /folks/1.json
  def update
    respond_to do |format|
      if @folk.update(folk_params)
        format.html { redirect_to @folk, notice: "Folk was successfully updated." }
        format.json { render :show, status: :ok, location: @folk }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @folk.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /folks/1 or /folks/1.json
  def destroy
    @folk.destroy
    respond_to do |format|
      format.html { redirect_to folks_url, notice: "Folk was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_folk
      @folk = Folk.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def folk_params
      params.require(:folk).permit(:first_name, :last_name, :email, :mobile_number, :twitter, :date_of_joining, :nick_name)
    end
end
