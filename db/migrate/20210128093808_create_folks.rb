class CreateFolks < ActiveRecord::Migration[5.2]
  def change
    create_table :folks do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :mobile_number
      t.string :date_of_joining
      t.string :twitter
      t.string :nick_name

      t.timestamps
    end
  end
end
